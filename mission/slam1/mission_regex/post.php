<?php
/**
 * Created by PhpStorm.
 * User: Yves-Quentin
 * Date: 29/03/2018
 * Time: 11:40
 */

//  ETAPE 1
$phrase = "Bonjour mon amis";
echo($phrase);
if (strpos($phrase, 'amis') !== false) {
    echo 'Cette phrase contient amis';
}

//  ETAPE 2
$phrase = "j’adore la musique";

if (strpos($phrase, 'musique') !== false) {
    echo str_replace("musique","MUSIQUE",$phrase);
}


//  ETAPE 3
$str = "hello";
echo $str[0] ;

if (strpos($str[0], 'a'or 'e' or 'i' or 'o' or 'u' or 'y') !== false) {
    echo  " Cette lettre est une voyelle" ;

}
if (strpos($str[0], 'a'or 'e' or 'i' or 'o' or 'u' or 'y') == false) {
    echo  "  Cette lettre n'est une voyelle" ;
}

// ETAPE 4

$string = '​j’adore la programmation';
$test = explode(' ', $string);
$last_word = array_pop($test);

echo $last_word;

//  ETAPE 5
$phrase = "youhouD";


if (strpos($phrase,"D" ) !== false) {
    echo  " Cette phrase contient un emoji" ;
}

if (strpos($phrase,"D" ) == false) {
    echo  " Cette phrase ne contient pas de emoji" ;
}

$mail1 = "titi@gmail.com";
$mail2 = "hoho.hihi.net";
$mail3 = "kiki@lolo";

    if(filter_var( $mail1,FILTER_VALIDATE_EMAIL)) {
        echo "L'adresse est valide";
    }
    else {
        echo "L'adresse n'est pas valide";
    }

// ETAPE 6
$NomDeDomaine = "cdm-sio.fr";
$NomDeDomaine2 = "cdm-sio.com";
if(strpos( $NomDeDomaine, 'sio.fr') !== false){
    echo "Le nom de domaine est valide";
}
else {
    echo "Le nom de domaine est incorrecte";
}


// ETAPE 7
$phrase = "LILI";
if (strpos($phrase, 'LILI') !== false) {
    echo str_replace("LILI","LULU",$phrase);
}

?>



