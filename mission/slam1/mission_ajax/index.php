<?php

if (isset($_POST['email'])) {
    $host = "localhost";
    $db_name = "missionajax";
    $db_user = "root";
    $db_mdp = "";
    try {
        $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
        $datta = new PDO('mysql:host=' . $host . ';dbname=' . $db_name . '', '' . $db_user . '', '' . $db_mdp . '', $pdo_options);
    } catch (Exception $e) {
        die('Erreur : ' . $e->getMessage());
    }

    $stmt = $datta->prepare("SELECT email FROM client WHERE email LIKE ?"); // requete prepare
    $stmt->execute(array($_POST['email'] . '%')); // execute la requete
    if ($stmt->rowCount() > 0) { // retourne une colonne depuis la ligne suivante d'un jeu de résultats
        $fetch = $stmt->fetchAll();
        for ($i = 0; $i < $stmt->rowCount(); $i++) {
            $data[] = array(
                "email" => $fetch[$i]['email']
            );
        }
        echo json_encode($data);
        return;
    } else {
        echo "Aucun ne correspond";
        return;
    }

}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
</head>

<body>

<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
Email user :
<input type="email" name="email" id="email" pattern="^[a-z]+[.+]+[a-z]+@hautil.net$"
       placeholder="prenom.nom@hautil.net">
<input id="btn2" type="submit" value="valider Email">
<div style="position:absolute;border:1px solid red;width:170px;left:90px;" id="result"></div>
</body>
</html>
<script>
    $( "#email" ).keyup(function() {
        $("#result").html(" ");
        var email = $("#email").val();
        if(email.length > 0) {
            $.ajax({
                type: "POST",
                data: {email: email},
                url: "index.php",
                success: function (result) {
                    result = JSON.parse(result);
                    for (var i = 0; i < result.length; i++) {
                        $("#result").append(result[i].email + "<br>");
                    }
                }
            });
        }
    });

</script>

