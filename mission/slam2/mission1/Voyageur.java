public class Voyageur {
    private String nom;
    private int age;
    private String categorie;

    public Voyageur(){
        this.nom = "Anis";
        this.age = 18;
        this.categorie = "Adulte";
    }

    public Voyageur(String nom, int age){
        setAge(age);
        setNom(nom);
        setCategorie();
    }

    public String getNom(){
        return this.nom;
    }
    public int getAge(){
        return this.age;
    }
    public String getCategorie(){
        return this.categorie;
    }
    public void setNom(String nom){
        if(nom.length() > 2) {
            this.nom = nom;
        } else {
            this.nom = "Anis";
        }
    }

    public void setAge(int age){
        if(age > 0){
            this.age = age;
        } else {
            this.age = 18;
        }
        setCategorie();
    }

    private void setCategorie(){
        if(this.age < 2){
            this.categorie = "nourisson";
        } else if(this.age >= 2 && this.age < 12){
            this.categorie = "enfant";
        } else if(this.age >= 12 && this.age <= 17){
            this.categorie = "adolescent";
        } else if(this.age >= 18 && this.age <= 55){
            this.categorie = "adulte";
        } else if(this.age > 55){
            this.categorie = "sénior";
        }
    }



}
