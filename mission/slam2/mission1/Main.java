import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Voyageur voyageur  = new Voyageur();

        System.out.println("Veuillez saisir votre nom:");
        voyageur.setNom(sc.next());

        System.out.println("Veuillez saisir votre age:");
        int age = sc.nextInt();
        voyageur.setAge(age);

        System.out.println("Voyageur " + voyageur.getNom() + " agé de " + voyageur.getAge() + " ans de la catégorie: " + voyageur.getCategorie());
    }
}
