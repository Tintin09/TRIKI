package dao;

import modele.Professeur;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ProfesseurDao {

    private Professeur professeur;

    public Connection getConnexion() {
        Connection conn = null;
        String driver = "org.mariadb.jdbc.Driver";
        String url = "jdbc:mariadb://localhost:3306/javamission6";
        String user = "root";
        String pwd = "";
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, user, pwd);
            System.out.println("connection ok");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }

    public void insertProfesseur(Professeur p) throws Exception {
        String req3 = "INSERT INTO professeur VALUES (?,?,?)";
        PreparedStatement pstmt = this.getConnexion().prepareStatement(req3);
        pstmt.setInt(1, p.getId());
        pstmt.setString(2, p.getNom());
        pstmt.setString(3, p.getSpecialite());
        pstmt.executeUpdate();
    }

    public void deleteProfesseur(Professeur p) throws Exception {
        PreparedStatement pstmt = this.getConnexion().prepareStatement("DELETE FROM professeur WHERE id = ?");
        pstmt.setInt(1, p.getId());
        pstmt.executeUpdate();
    }

    public Professeur getProfesseurById(int id) throws Exception {
        Professeur res = null;
        PreparedStatement pstmt = this.getConnexion().prepareStatement("SELECT id,nom,specialite FROM professeur WHERE id = ?");
        pstmt.setInt(1, id);
        ResultSet result = pstmt.executeQuery();
        while (result.next()) {
            res = new Professeur(result.getInt(1),result.getString(2),result.getString(3));
        }
        return res;
    }

    public ArrayList<Professeur> getProfesseursBySpecialite(String s) throws Exception {
        ArrayList<Professeur> list = new ArrayList<Professeur>();
        PreparedStatement pstmt = this.getConnexion().prepareStatement("SELECT id,nom,specialite FROM professeur WHERE specialite = ?");
        pstmt.setString(1, s);
        ResultSet result = pstmt.executeQuery();
        while (result.next()) {
            list.add(new Professeur(result.getInt(1),result.getString(2),result.getString(3)));
        }
        return list;
    }
}
