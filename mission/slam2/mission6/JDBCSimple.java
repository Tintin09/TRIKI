import java.sql.*;

public class JDBCSimple {
    public static void main(String[] args) throws Exception {

        Connection connection;
        Class.forName("org.mariadb.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/javamission6","root", "");


        Statement stmt = connection.createStatement();

        /**
        String req1 = "INSERT INTO professeur (nom,specialite) VALUES ('Abdelmoula','SLAM')";
        stmt.executeUpdate(req1);
         */

        String req2 = "SELECT nom, specialite FROM professeur";
        ResultSet result = stmt.executeQuery(req2);
        while(result.next()){
            System.out.print( "Nom : "+ result.getString(1));
            System.out.print( "Spécialité : "+ result.getString(2));
        }
        result.close();
        stmt.close();


    }
}