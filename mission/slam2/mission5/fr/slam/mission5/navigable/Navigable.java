package fr.slam.mission5.navigable;

public interface Navigable {

    public void accoster();
    public void naviguer();
    public void couler();


}
