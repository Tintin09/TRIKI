package fr.slam.mission5.navigable;

import fr.slam.mission5.animal.Canard;
import fr.slam.mission5.vehicule.Bateau;

import java.util.ArrayList;

public class NavigableManager {
    private static ArrayList<Navigable> navigables;

    public static void initialise() {
        navigables = new ArrayList<>();

        navigables.add(new Canard("CH1", 10, "test"));
        navigables.add(new Canard("CH1", 10, "test"));
        navigables.add(new Canard("CH1", 10, "test"));

        navigables.add(new Bateau("BAT1","blanc","B1"));
        navigables.add(new Bateau("BAT2","blanc","B2"));
        navigables.add(new Bateau("BAT3","blanc","B3"));
    }

    public static ArrayList<Navigable> getNavigable() {
        return navigables;
    }
}
