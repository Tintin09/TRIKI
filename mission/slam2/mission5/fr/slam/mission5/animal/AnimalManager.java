package fr.slam.mission5.animal;

import java.util.ArrayList;

public class AnimalManager {

    private static ArrayList<Animal> animaux;

    public static void initialise() {
        animaux = new ArrayList<>();
        animaux.add(new Chien("CH1", 10, "test"));
        animaux.add(new Chien("CH2", 10, "test"));
        animaux.add(new Chien("CH3", 10, "test"));
        animaux.add(new Canard("C1", 10, "test"));
        animaux.add(new Canard("C2", 10, "test"));
        animaux.add(new Canard("C3", 10, "test"));
    }

    public static ArrayList<Animal> getAnimaux() {
        return animaux;
    }
}
