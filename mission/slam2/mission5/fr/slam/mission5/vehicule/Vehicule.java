package fr.slam.mission5.vehicule;

public abstract class Vehicule {

    private String marque;
    private String couleur;
    private String modele;
    protected String action;

    Vehicule(String marque, String couleur, String modele){
        this.marque = marque;
        this.couleur = couleur;
        this.modele = modele;
    }
    public void demarrer(){
        this.action = "demarrer";
    }

    public void freiner(){
        this.action = "freiner";
    }

    public String getModele(){
        return modele;
    }
}
