package fr.slam.mission5.vehicule;


import fr.slam.mission5.navigable.Navigable;

public class Bateau extends Vehicule implements Navigable {
    private int volume;

    public Bateau(String marque, String couleur, String modele){
        super(marque,couleur,modele);
    }


    public String toString(){
        return "Bateau modele: " + getModele();
    }

    public void accoster(){
        this.action = "accoster";
    }

    public void naviguer(){
        this.action = "navigue";
    }

    public void couler(){
        this.action = "coule";
    }
}
