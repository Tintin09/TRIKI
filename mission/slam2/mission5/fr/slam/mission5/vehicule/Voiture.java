package fr.slam.mission5.vehicule;

public class Voiture extends Vehicule {

    private int nbPlace;
    private boolean station;

    Voiture(String marque, String couleur, String modele){
        super(marque,couleur,modele);
    }


    public void stationner(){
        this.station = true;
    }
}
