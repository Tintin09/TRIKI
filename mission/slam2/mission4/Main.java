import eu.siohautil.base.AdressePostal;
import eu.siohautil.base.AgenceVoyage;
import eu.siohautil.base.Voyageur;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        AgenceVoyage agence = new AgenceVoyage("AnisAgence",new AdressePostal("voie","ville","95"));
        System.out.println(agence);
        boolean continuer = true;
        System.out.println("Fin de l'initialisation, que voulez-vous faire maintenant?");
        while(continuer) {
            System.out.println("1- Ajouter un nouveau voyageur");
            System.out.println("2- Recherher un voyageur par son nom");
            System.out.println("3- Supprimer un utilisateur par son nom");
            System.out.println("4- En savoir plus sur l'agence de base");
            System.out.println("5- Quitter l'application");
            int numero = sc.nextInt();
            String nom;
            Voyageur voyageur;
            sc.nextLine();
            switch (numero){
                case 5:
                    continuer = false;
                    System.out.println("Fin du programme.");
                    break;
                case 1:
                    System.out.println("Nom du voyageur");
                    nom = sc.nextLine();
                    System.out.println("Age du voyageur");
                    int age = sc.nextInt();
                    agence.newVoyageur(new Voyageur(nom,age));
                    System.out.println("Succès!");
                    break;
                case 2:
                    System.out.println("Nom du voyageur");
                    nom = sc.nextLine();
                    voyageur = agence.getVoyageurs(nom);
                    if(voyageur != null){
                        System.out.println(voyageur);
                    } else {
                        System.out.println("eu.siohautil.base.Voyageur introuvable");
                    }
                    break;
                case 3:
                    System.out.println("Nom du voyageur");
                    nom = sc.nextLine();
                    voyageur = agence.deleteVoyageur(nom);
                    if(voyageur != null){
                        System.out.println(voyageur);
                    } else {
                        System.out.println("eu.siohautil.base.Voyageur introuvable");
                    }
                    break;
                case 4:
                    System.out.println(agence);

            }
        }
    }
}
