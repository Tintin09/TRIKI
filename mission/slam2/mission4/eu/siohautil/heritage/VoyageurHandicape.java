package eu.siohautil.heritage;
import eu.siohautil.base.Voyageur;

/**Classe VoyageurHandicape
 * Cette classe permet de créer un voyageur avec un handicape.
 * @see Voyageur
 * @author Anis
 * @version 1.0
 */
public class VoyageurHandicape extends Voyageur {
    private String handicape;

    /** Constructeur
     *
     * @param handicape une chaîne de caractère
     * @param nom une chaîne de caractère
     * @param age un entier
     */
    public VoyageurHandicape(String handicape,  String nom, int age){
        super(nom,age);
        this.handicape = handicape;
    }

    /**Récuperer l'handicape
     *
     * @return handicape
     */
    public String getHandicape(){
        return this.handicape;
    }

    /** Modifier l'handicape
     *
     * @param handicape une chaîne de caractère
     */
    public void setHandicape(String handicape){
        this.handicape = handicape;
    }


    /** Afficher l'objet
     *
     * @return l'objet
     */
    @Override
    public String toString(){
        return "Ce voyageur possède l'handicape suivant: " + handicape + "\n" +
                super.toString();
    }

}
