package eu.siohautil.heritage;
import eu.siohautil.base.Voyageur;

/**Classe VoyageurPrivilege
 * Cette classe permet de créer un voyageur avec un code de privilèges
 * @see Voyageur
 * @auhor Anis
 * @version 1.0
 */
public class VoyageurPrivilege extends Voyageur {
    private int code;

    /**Constructeur
     *
     * @param code un entier
     * @param nom une chaîne de caractère
     * @param age un entier
     */
    public VoyageurPrivilege(int code, String nom, int age){
        super(nom,age);
        this.code = code;
    }

    /**Récuperer le code
     *
     * @return code
     */
    public int getCode(){
        return this.code;
    }

    /**Mettre à jour le code
     *
     * @param code un entier
     */
    public void setCode(int code){
        this.code = code;
    }

    /**Afficher l'objet
     *
     * @return l'objet
     */
    @Override
    public String toString(){
        return "Ce voyageur possède le code de priviliège suivant: " + code + "\n" +
                super.toString();
    }

}
