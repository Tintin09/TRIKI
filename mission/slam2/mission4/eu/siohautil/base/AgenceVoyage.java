package eu.siohautil.base;
import eu.siohautil.heritage.VoyageurHandicape;
import eu.siohautil.heritage.VoyageurPrivilege;

import java.util.ArrayList;

/**
 * Classe AgenceVoyage
 *
 * Cette classe permet de créer une agence de voyage qui sera composé de voyageurs
 * @see Voyageur
 * @author Anis
 * @version 1.0
 */
public class AgenceVoyage {
    private String nom;
    private AdressePostal adresse = null;
    private ArrayList<Voyageur> voyageurs = new ArrayList<>();

    /**
     * Le constructeur
     *
     * @param nom     une chaîne de caractère
     * @param adresse une adresse postal
     */
    public AgenceVoyage(String nom, AdressePostal adresse) {
        this.nom = nom;
        this.adresse = adresse;
        Voyageur voyageur = null;
        this.voyageurs.add(voyageur = new VoyageurPrivilege(15,"Anis", 17));
        this.voyageurs.add(voyageur = new VoyageurHandicape("Malade","Anis2", 17));
        this.voyageurs.add(voyageur = new Voyageur("Anis3", 17));
        this.voyageurs.add(voyageur = new Voyageur("Anis4", 17));
        this.voyageurs.add(voyageur = new Voyageur("Anis5", 17));
    }

    /**
     * Récuperer le nom
     *
     * @return nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Mettre le nom
     *
     * @param nom une chaîne de caractère
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Récuperer l'adresse
     *
     * @return adresse
     */
    public AdressePostal getAdresse() {
        return adresse;
    }

    /**
     * Mettre l'adresse
     *
     * @param adresse
     */
    public void setAdresse(AdressePostal adresse) {
        this.adresse = adresse;
    }

    /**
     * Créer un nouveau voyageur et l'ajouter dans la liste
     *
     * @param voyageur de type Voyageur
     */
    public void newVoyageur(Voyageur voyageur) {
        this.voyageurs.add(voyageur);
    }

    /**
     * Récuperer un voyageur selon le nom
     *
     * @param nom une chaîne de caractère
     * @return un voyageur
     */
    public Voyageur getVoyageurs(String nom) {
        for (int i = 0; i < this.voyageurs.size(); i++) {
            if (this.voyageurs.get(i).getNom().equals(nom)) {
                return this.voyageurs.get(i);
            }
        }
        return null;
    }

    /**
     * Supprimer un voyageur selon le nom
     *
     * @param nom une chaîne de caractère
     * @return le voyageur
     */
    public Voyageur deleteVoyageur(String nom) {
        for (int i = 0; i < this.voyageurs.size(); i++) {
            if (this.voyageurs.get(i).getNom().equals(nom)) {
                Voyageur voyageur = this.voyageurs.get(i);
                this.voyageurs.remove(i);
                return voyageur;
            }
        }
        return null;
    }

    /**
     * Afficher tout les voyageurs
     */

    public String toString(){
        String nom = "Le nom est: " + getNom() + "\n";
        String adresse = "L'adresse est: " + this.adresse.toString() + "\n" + " et possède les voyageurs suivants: " + "\n";
        String voyageurs = "";
        for (int i = 0; i < this.voyageurs.size(); i++) {
            voyageurs = voyageurs + this.voyageurs.get(i).toString() + "\n";
        }
        return nom + adresse + voyageurs;
    }
}
