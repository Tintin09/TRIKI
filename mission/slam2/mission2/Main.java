import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Voyageur voyageur  = new Voyageur();

        System.out.println("Veuillez saisir votre nom:");
        voyageur.setNom(sc.nextLine());

        System.out.println("Veuillez saisir votre age:");
        int age = sc.nextInt();
        voyageur.setAge(age);
        sc.nextLine();

        voyageur.affiche();

        System.out.println("Modifier la voie:");
        String voie = sc.nextLine();

        System.out.println("Modifier la ville:");
        String ville = sc.nextLine();

        System.out.println("Modifier le code postal:");
        String postal = sc.nextLine();
        voyageur.setAdresse(voie,ville,postal);
        voyageur.affiche();


        System.out.println("Voulez-vous modifier le nom de la ville ?");
        if(sc.nextBoolean() == true) {
            System.out.println("Modifier la ville:");
            voyageur.setVille(sc.next());
            voyageur.affiche();
        }
        System.out.println("Voulez-vous enregistrer un bagage?");
        if(sc.nextBoolean() == true) {
            System.out.println("Quel est le numéro du bagage?:");
            int numbagage = sc.nextInt();
            sc.nextLine();
            System.out.println("Quel est la couleur du bagage?:");
            String coulbagage = sc.nextLine();
            System.out.println("Quel est le poids du bagage?:");
            double poidsbagage = sc.nextDouble();
            Bagage bagage = new Bagage(numbagage,coulbagage,poidsbagage);
            voyageur.setBagage(bagage);
            voyageur.affiche();

            System.out.println("Voulez-vous modifier la couleur du bagage?");
            if(sc.nextBoolean() == true) {
                sc.nextLine();
                System.out.println("Quel est la couleur du bagage?:");
                voyageur.setBagageCouleur(sc.nextLine());
                voyageur.affiche();
            }

            System.out.println("Voulez-vous terminer ou supprimer le bagage ?");
            if(sc.nextBoolean() == true){
                voyageur.deleteBagage();
                voyageur.affiche();
            } else {
                voyageur.affiche();
            }
        } else {
            voyageur.affiche();
            System.out.println("Fin du programme");
        }
    }
}

