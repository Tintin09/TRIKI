public class Voyageur {
    private String nom;
    private int age;
    private String categorie;
    AdressePostal adressePostal  = null;
    Bagage bagage;

    public Voyageur(){
        this.nom = "Anis";
        this.age = 18;
        this.categorie = "Adulte";
    }

    public Voyageur(String nom, int age){
        setAge(age);
        setNom(nom);
        setCategorie();
    }

    public void affiche(){
        System.out.println("Voyageur " + getNom() + " agé de " + getAge() + " ans de la catégorie: " + getCategorie());

        if(adressePostal != null){
            adressePostal.afficher();
        }
        if(bagage != null){
            bagage.afficher();
        }

    }

    public String getNom(){
        return this.nom;
    }
    public int getAge(){
        return this.age;
    }
    public String getCategorie(){
        return this.categorie;
    }
    public void setNom(String nom){
        if(nom.length() > 2) {
            this.nom = nom;
        } else {
            this.nom = "Anis";
        }
    }

    public void setAge(int age){
        if(age > 0){
            this.age = age;
        } else {
            this.age = 18;
        }
        setCategorie();
    }

    private void setCategorie(){
        if(this.age < 2){
            this.categorie = "nourisson";
        } else if(this.age >= 2 && this.age < 12){
            this.categorie = "enfant";
        } else if(this.age >= 12 && this.age <= 17){
            this.categorie = "adolescent";
        } else if(this.age >= 18 && this.age <= 55){
            this.categorie = "adulte";
        } else if(this.age > 55){
            this.categorie = "sénior";
        }
    }

    public void setAdresse(String voie, String ville, String postal){
        adressePostal  = new AdressePostal(voie,ville,postal);
    }


    public void setVille(String ville){
        adressePostal.setVille(ville);
    }
    public void setBagage(int numero,String couleur,double poids){
        this.bagage = new Bagage(numero,couleur,poids);
    }

    public void setBagage(Bagage bagage){
        this.bagage = bagage;
    }

    public Bagage getBagage() {
        return bagage;
    }

    public void setBagageCouleur(String couleur){
        bagage.setCouleur(couleur);
    }

    public void deleteBagage(){
        this.bagage = null;
    }


}
