public class AdressePostal {

    private String voie;
    private String ville;
    private String postal;

    AdressePostal(String voie, String ville, String postal){
        this.voie = voie;
        this.ville = ville;
        this.postal = postal;
    }

    public void afficher(){
            System.out.println("Adresse: " + this.voie + ", " + this.ville + " " + this.postal);
    }

    public void setVoie(String voie){
        this.voie = voie;
    }

    public void setVille(String ville){
        this.ville = ville;
    }

    public void setPostal(String postal){
        this.postal = postal;
    }

    public String getVoie(){
        return voie;
    }

    public String getVille(){
        return ville;
    }

    public String getPostal(){
        return postal;
    }


}
