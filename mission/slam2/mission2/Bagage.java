public class Bagage {
    private int numero;
    private String couleur;
    private double poids;

    Bagage(int numero,String couleur, double poids){
        this.numero = numero;
        this.couleur = couleur;
        this.poids = poids;
    }
    public void afficher(){
            System.out.println("Infomartion bagage : Bagage numéro " + this.numero + " de la couleur " + this.couleur + " pensant " + this.poids + " kg.");
    }
    public void setNumero(int numero){
        this.numero = numero;
    }
    public void setCouleur(String couleur){
        this.couleur = couleur;
    }
    public void setPoids(double poids){
        this.poids = poids;
    }

    public double getPoids(){
        return this.poids;
    }
    public String getCouleur(){
        return this.couleur;
    }
    public int getNumero(){
        return this.numero;
    }


}
