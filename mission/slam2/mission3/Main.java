import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        AgenceVoyage agence = new AgenceVoyage("AnisAgence",new AdressePostal("voie","ville","95"));
        agence.afficher();
        boolean continuer = true;
        System.out.println("Fin de l'initialisation, que voulez-vous faire maintenant?");
        while(continuer) {
            System.out.println("1- Ajouter un nouveau voyageur");
            System.out.println("2- Recherher un voyageur par son nom");
            System.out.println("3- Supprimer un utilisateur par son nom");
            System.out.println("4- En savoir plus sur l'agence de voyage");
            System.out.println("5- Quitter l'application");
            int numero = sc.nextInt();
            String nom;
            Voyageur voyageur;
            sc.nextLine();
            switch (numero){
                case 5:
                    continuer = false;
                    System.out.println("Fin du programme.");
                    break;
                case 1:
                    System.out.println("Nom du voyageur");
                    nom = sc.nextLine();
                    System.out.println("Age du voyageur");
                    int age = sc.nextInt();
                    agence.newVoyageur(new Voyageur(nom,age));
                    System.out.println("Succès!");
                    break;
                case 2:
                    System.out.println("Nom du voyageur");
                    nom = sc.nextLine();
                    voyageur = agence.getVoyageurs(nom);
                    if(voyageur != null){
                        voyageur.affiche();
                    } else {
                        System.out.println("Voyageur introuvable");
                    }
                    break;
                case 3:
                    System.out.println("Nom du voyageur");
                    nom = sc.nextLine();
                    voyageur = agence.deleteVoyageur(nom);
                    if(voyageur != null){
                        voyageur.affiche();
                    } else {
                        System.out.println("Voyageur introuvable");
                    }
                    break;
                case 4:
                    agence.afficher();

            }
        }
    }
}
