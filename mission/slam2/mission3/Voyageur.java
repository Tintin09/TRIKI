/**
 * Classe Voyageur
 * <p>
 * Cette classe permet de créer un voyageur
 *
 * @author Anis
 * @version 1.0
 */

public class Voyageur {

    private String nom;
    private int age;
    private String categorie;
    AdressePostal adressePostal = null;
    Bagage bagage;

    public Voyageur() {
        this.nom = "Anis";
        this.age = 18;
        this.categorie = "Adulte";
    }

    /**
     * Le constructeur
     *
     * @param nom une chaîne de caractère
     * @param age un entier
     */
    public Voyageur(String nom, int age) {
        setAge(age);
        setNom(nom);
        setCategorie();
    }

    /**
     * Afficher le voyageur
     * Affiche le voyageur
     */
    public void affiche() {
        System.out.println("Voyageur " + getNom() + " agé de " + getAge() + " ans de la catégorie: " + getCategorie());

        if (adressePostal != null) {
            adressePostal.afficher();
        }
        if (bagage != null) {
            bagage.afficher();
        }

    }

    /**
     * Récuperer le nom
     *
     * @return nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Récuperer l'age
     *
     * @return age
     */
    public int getAge() {
        return this.age;
    }

    /**
     * Récuperer la catégorie
     *
     * @return categorie
     */
    public String getCategorie() {
        return this.categorie;
    }

    /**
     * Mettre le nom
     *
     * @param nom une chaîne de caractère
     */
    public void setNom(String nom) {
        if (nom.length() > 2) {
            this.nom = nom;
        } else {
            this.nom = "Anis";
        }
    }

    /**
     * Mettre l'age
     *
     * @param age un entier
     */
    public void setAge(int age) {
        if (age > 0) {
            this.age = age;
        } else {
            this.age = 18;
        }
        setCategorie();
    }

    /**
     * Mettre la catégorie
     */
    private void setCategorie() {
        if (this.age < 2) {
            this.categorie = "nourisson";
        } else if (this.age >= 2 && this.age < 12) {
            this.categorie = "enfant";
        } else if (this.age >= 12 && this.age <= 17) {
            this.categorie = "adolescent";
        } else if (this.age >= 18 && this.age <= 55) {
            this.categorie = "adulte";
        } else if (this.age > 55) {
            this.categorie = "sénior";
        }
    }

    /**
     * Mettre l'adresse
     *
     * @param voie   une chaîne de caractère
     * @param ville  une chaîne de caractère
     * @param postal une chaîne de caractère
     */
    public void setAdresse(String voie, String ville, String postal) {
        adressePostal = new AdressePostal(voie, ville, postal);
    }

    /**
     * Mettre la ville
     *
     * @param ville une chaîne de caractère
     */
    public void setVille(String ville) {
        adressePostal.setVille(ville);
    }

    /**
     * Mettre un bagage
     *
     * @param bagage de type bagage
     */
    public void setBagage(int numero, String couleur, double poids) {
        this.bagage = new Bagage(numero, couleur, poids);
    }

    public void setBagage(Bagage bagage) {
        this.bagage = bagage;
    }

    /**
     * Récuperer un bagage
     *
     * @return bagage
     */

    public Bagage getBagage() {
        return bagage;
    }

    /**
     * Mettre une couleur à un bagage
     *
     * @param couleur une chaîne de caractère
     */
    public void setBagageCouleur(String couleur) {
        bagage.setCouleur(couleur);
    }

    /**
     * Supprime le bagage
     */
    public void deleteBagage() {
        this.bagage = null;
    }


}
