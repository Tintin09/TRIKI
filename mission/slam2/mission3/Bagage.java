/**
 * Classe Bagage
 * <p>
 * Cette classe permet de créer un bagage associé à un voyageur
 *
 * @author Anis
 * @version 1.0
 * @see Voyageur
 */
public class Bagage {
    private int numero;
    private String couleur;
    private double poids;


    /**
     * Le constructeur
     *
     * @param numero  un entier
     * @param couleur une chaîne de caractère
     * @param poids   un réel
     */
    Bagage(int numero, String couleur, double poids) {
        this.numero = numero;
        this.couleur = couleur;
        this.poids = poids;
    }

    /**
     * Afficher les informations du bagage
     */
    public void afficher() {
        System.out.println("Infomartion bagage : Bagage numéro " + this.numero + " de la couleur " + this.couleur + " pensant " + this.poids + " kg.");
    }


    /**
     * Mettre le numéro
     *
     * @param numero un entier
     */

    public void setNumero(int numero) {
        this.numero = numero;
    }


    /**
     * Mettre la couleur
     *
     * @param couleur une chaîne de caractère
     */
    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }


    /**
     * Mettre le poid
     *
     * @param poids un réel
     */
    public void setPoids(double poids) {
        this.poids = poids;
    }

    /**
     * Récuperer le poid
     *
     * @return poids
     */
    public double getPoids() {
        return this.poids;
    }

    /**
     * Récuperer la couleur
     *
     * @return couleur
     */
    public String getCouleur() {
        return this.couleur;
    }

    /**
     * Récuperer le numéro
     *
     * @return numero
     */
    public int getNumero() {
        return this.numero;
    }


}
