import java.util.ArrayList;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Voyageur> voyageurs = new ArrayList<>();

        boolean continuer = true;
        while (continuer) {
            System.out.println("Veuillez saisir votre nom:");
            String nom = sc.nextLine();

            System.out.println("Veuillez saisir votre age:");
            int age = sc.nextInt();
            sc.nextLine();
            Voyageur voyageur = new Voyageur(nom, age);
            voyageurs.add(voyageur);

            System.out.println("Modifier la voie:");
            String voie = sc.nextLine();

            System.out.println("Modifier la ville:");
            String ville = sc.nextLine();

            System.out.println("Modifier le code postal:");
            String postal = sc.nextLine();
            voyageur.setAdresse(voie, ville, postal);

            System.out.println("Voulez-vous enregistrer un bagage?");
            if (sc.nextBoolean()) {
                sc.nextLine();
                System.out.println("Quel est le numéro du bagage?:");
                int numbagage = sc.nextInt();
                sc.nextLine();
                System.out.println("Quel est la couleur du bagage?:");
                String coulbagage = sc.nextLine();
                System.out.println("Quel est le poids du bagage?:");
                double poidsbagage = sc.nextDouble();
                Bagage bagage = new Bagage(numbagage, coulbagage, poidsbagage);
                voyageur.setBagage(bagage);
            }
            sc.nextLine();
            System.out.println("Voulez-vous saisir un autre voyageur?");
            continuer = sc.nextBoolean();
            sc.nextLine();
        }

        for(int i = 0; i < voyageurs.size(); i++){
            voyageurs.get(i).affiche();
        }
        continuer = true;
        System.out.println("Rechercher un utilisateur et l'afficher");
        String rechercheNom = sc.nextLine();
        for(int i = 0; i < voyageurs.size(); i++){
            if(voyageurs.get(i).getNom().equals(rechercheNom)){
                voyageurs.get(i).affiche();
                continuer = false;
                break;
            }
        }
        if(continuer){
            System.out.println("Voyageur introuvable.");
        }

        continuer = true;
        System.out.println("Rechercher un utilisateur et récuperer sa position et le supprimer.");
        rechercheNom = sc.nextLine();
        for(int i = 0; i < voyageurs.size(); i++){
            if(voyageurs.get(i).getNom().equals(rechercheNom)){
                voyageurs.get(i).affiche();
                System.out.println(rechercheNom + " va être supprimé. Voici la position: " + voyageurs.indexOf(i));
                voyageurs.remove(i);
                continuer = false;
                break;
            }
        }
        if(continuer){
            System.out.println("Voyageur introuvable.");
        } else {
            for(int i = 0; i < voyageurs.size(); i++){
                voyageurs.get(i).affiche();
            }
        }

    }
}
