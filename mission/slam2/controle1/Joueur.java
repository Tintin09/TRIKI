public class Joueur{
	private String nom;
	private String prenom;
	private int numero;
	
	Joueur(){
		this.nom = "Triki";
		this.prenom = "Anis";
		this.numero = 10;
	}
	
	Joueur(String nom, String prenom, String numero){
		this.nom = nom;
		this.prenom = prenom;
		this.numero = numero;
	}
	public String getNom(){
		return this.nom;
	}
	public String getPrenom(){
		return this.prenom;
	}
	public int getNumero(){
		return this.numero;
	}
	
	public void setNom(String nom){
		this.nom = nom;
	}
	public void setPrenom(String prenom){
		this.prenom = prenom;
	}
	public void setNumero(int numero){
		this.numero = numero;
	}
	public void afficher(){
		System.out.println("Le nom est: " + this.nom);
		System.out.println("Le prenom est: " + this.prenom);
		System.out.println("Le numéro est: " + this.numero);
	}
}