
public class Equipe{
	private String nom;
	private String coach;
	private String ville;
	private ArrayList<Joueur> joueurs = new ArrayList<Joueur>();
	
	Equipe(String nom,String ville){
		this.nom = nom;
		this.ville = ville;
	}
	
	Equipe(String nom, String ville, String coach){
		this.nom = nom;
		this.ville = ville;
		this.coach = coach;
	}
	
	public String getCoach(){
		return this.coach;
	}
	public String getVille(){
		return this.ville;
	}
	public String getJoueurs(){
		return this.joueurs;
	}
	public String getNom(){
		return this.nom;
	}
	
	public void setCoach(String coach){
		this.coach = coach;
	}
	public void setVille(String ville){
		this.ville = ville;
	}
	public void setNom(String nom){
		this.coach = nom;
	}
	public void setJoueurs(Joueur joueurs){
		this.joueurs = joueurs;
	}
	
	public void addJoueur(Joueur joueur){
		this.joueurs.add(joueur);
	}

	public void removeJoueur(int numero){
		for(int i = 0; i < joueurs.size(); i++){
			if(joueurs.get(i).getNumero() == numero){
				joueurs.remove(i);
				break;
			}
		}
	}
	
	public void afficher(){
		System.out.println("Le nom est: " + this.nom);
		System.out.println("Le coach est: " + this.coach);
		System.out.println("La ville est: " + this.ville);
		System.out.println("Voici la liste des joueurs");
		for(int i = 0;i < joueurs.size(); i++){
			joueurs.get(i).afficher();
		}
	}
}
			

	
	
		