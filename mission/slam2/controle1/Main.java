public class Main {
    public static void main(String[] args) {
		Equipe barcelone = new Equipe("FC Barcelone","Barcelone","Valverde");
		Joueur anis = new Joueur("Anis","Triki",10);
		Joueur triki = new Joueur("Triki","Anis",11);
		
		barcelone.addJoueur(anis);
		barcelone.addJoueur(triki);
		
		barcelone.afficher();
		
    }
}