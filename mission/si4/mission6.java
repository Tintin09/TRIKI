/*
Algo:Mission6
variables: age[] tableau d'entier
           ageMin,ageMax entier
           moyenne réel
		   nom[] tableau de chaine de carractère
		   categorie Chaine de carractère
Debut
    nom <- {"Anis","Tcha","Monsieur","Lui","Autre"}
	Afficher "Commencons la liste des voyageurs"
	n <- age.taille
	Pour i indice de 0 à n pas 1
	    faire
		    Saisir age[i]
	Fin Pour
	Afficher "Voici la liste des voyageurs"
	moyenne <- 0
	ageMin <- age[0]
	ageMax <- age[0]
	
	Pour i indice de 0 à n pas 1
	    Si ageMin > age[i]
		    alors
			    ageMin <- age[i]
		Fin Si
		Si ageMax < age[i]
		    alors
			    ageMax = age[i]
		Fin Si
		
		categorie <- "null"
		Si age[i] < 2
		    alors
			    categorie <- "nourisson"
		Sinon Si age[i] >= 2 ET age[i] < 12
		    alors
			    categorie <- "enfant"
		Sinon Si age[i] >= 12 ET age[i] <= 55
		    alors
			    categorie <- "adulte"
		Sinon Si age[i] > 55
		    alors
			    categorie <- "senior"
		Fin Si
	    moyenne <- moyenne + age[i]
        Afficher "Monsieur " + nom[i] + " a " + age[i] + " ans, qui est un " + categorie"
	Fin Pour
	moyenne <- moyenne / nom
	Afficher moyenne
	Afficher ageMin
	Afficher ageMax
FIN
 */
import java.util.Scanner;
public class Mission6 {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        String nom[] = {"Anis","Tchatche","Monsieur","Lui","Autre"};
        int age[] = new int[5];
        System.out.println("Commencons la liste des voyageurs:");
        for(int i = 0; i < age.length; i++){
            System.out.println("Saisissez l'âge de: " + nom[i]);
            age[i] = sc.nextInt();
        }
        System.out.println("Voici la liste des voyageurs:");
        double moyenne = 0;
        int ageMin = age[0];
        int ageMax = age[0];
        for(int i = 0; i < age.length; i++){
            if(ageMin > age[i]){
                ageMin = age[i];
            }
            if(ageMax < age[i]){
                ageMax = age[i];
            }
            String categorie = "null";
            if(age[i] < 2){
                categorie = "nourisson";
            } else if(age[i] >= 2 && age[i] < 12){
                categorie = "enfant";
            } else if(age[i] >= 12 && age[i] <= 55){
                categorie = "adulte";
            } else if(age[i] > 55){
                categorie = "sénior";
            }
            moyenne = moyenne + age[i];
            System.out.println("Monsieur " + nom[i] + " a " + age[i] + " ans, qui est un " + categorie);
        }
        moyenne = moyenne / age.length;
        System.out.println("La moyenne d'age est de "+moyenne+" ans");
        System.out.println("Le plus petit a "+ageMin+" ans");
        System.out.println("Le plus grand a "+ageMax+" ans");
    }
}
