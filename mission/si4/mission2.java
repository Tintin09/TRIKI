import java.util.Scanner;
//Algo question 1
/*
Variables: nom,     réel
      quantite     entier
          prix     double

Debut:
   saisir nom
   saisir quantite
   saisir prix
   prix <- quantite x (prix x 1.196)
     Si quantite > 100
        alors
           prix <- prix x 0.9
        Fin si
        Afficher prix
FIN

 */
 // Question 1
public class MissionDeux{
    public static void main (String [] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir le nom du produit:");
        String nom = sc.nextLine();
        System.out.println("Veuillez saisir la quantité achetée:");
        int quantite = sc.nextInt();
        System.out.println("Veuillez saisir le prix HT:");
        //double prix = sc.nextDouble();
        double prix = quantite * (sc.nextDouble() * 1.196);
        if(quantite > 100){
        prix = prix * 0.9;
        }
        System.out.println("Le pric TTC est du " + nom + "est de " + prix + " euros" );
    }
    
    
    
    // Class finale 

import java.util.Scanner;

public class MissionDeux {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir votre age:");
        int age = sc.nextInt();
        if (age >= 18) {
            System.out.println("Veuillez saisir le nom du produit:");
            String nom = sc.next();
            System.out.println("Veuillez saisir la quantité achetée:");
            int quantite = sc.nextInt();
            double prix;
            double tva;
            boolean demanderprix;
            switch (nom) {
                case "PlayStation":
                    demanderprix = false;
                    prix = 399;
                    tva = 1.05;
                    break;
                case "iPhone X":
                    prix = 1119;
                    demanderprix = false;
                    tva = 1.05;
                    break;
                //Les produits avec une TVA de 5% sinon tout les autres à 20%
                default:
                    prix = 0;
                    demanderprix = true;
                    tva = 1.2;
                    break;
            }
            if (demanderprix) {
                System.out.println("Veuillez saisir le prix du produit:");
                prix = sc.nextDouble();
            }
            double prixTTC = quantite * (prix * tva);
            if (quantite > 100) {
                prixTTC = prixTTC * 0.9;
            }
            System.out.println("Le pric TTC est du " + nom + " est de " + prixTTC + " euros");
        } else {
            System.out.println("Tu ne peux pas utiliser cet outil.");
        }
    }
}