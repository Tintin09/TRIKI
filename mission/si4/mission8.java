/*
Algo:Mission8
    variables:
	x entier
	
	
	fonction Categorie(Entier age):Chaine de carractere
		variables locales:
			categorie Chaine de carractere
			age
		Debut	
			Si age < 2 alors:
				categorie <- "nourisson"
			Sinon si age >= 2 ET age < 12 alors:
				categorie <- "enfant"
			Sinon si age >= 12 ET age <= 55 alors:
				categorie <- "adulte"
			Sinon si age > 55 alors:
				categorie <- "sénior"
			Fin si
			Retourner categorie
		Fin 

	fonction Agemoyenne:
	fonction Agemin:
	fonction AgeMax:
	fonction Utilisateurs:

	Debut
		x <- 1
		Tant que x = 1 faire:
			Saisir nom
			Afficher Utilisateurs(nom)
			
			Saisir x
		Fin tant que
	Fin
FIN Mission8

*/

import java.util.Scanner;
public class Main {

    static String Categorie(int age){
        String categorie = "null";
        if(age < 2){
            categorie = "nourisson";
        } else if(age >= 2 && age < 12){
            categorie = "enfant";
        } else if(age >= 12 && age <= 55){
            categorie = "adulte";
        } else if(age > 55){
            categorie = "sénior";
        }
        return categorie;
    }
    static double AgeMoyenne(int[] notes){
        double moyenne = 0;
        for(int i = 0; i < notes.length; i++){
            moyenne = moyenne + notes[i];
        }
        moyenne = moyenne / notes.length;
        return moyenne;
    }
    static int AgeMin(int[] age) {
        int a = age[0];
        for (int i = 0; i < age.length; i++) {
            if (a > age[i]) {
                a = age[i];
            }
        }
        return a;
    }
    static int AgeMax(int[] age) {
        int a = age[0];
        for (int i = 0; i < age.length; i++) {
            if (a < age[i]) {
                a = age[i];
            }
        }
        return a;
    }
    static String Utilisateurs(String qui){
        String nom[] = {"Anis","Tchatche","Monsieur","Lui","Autre"};
        int age[] = {18,19,20,17,10};
        String a = "Le voyageurs est introuvable.";
        for(int i = 0; i < nom.length; i++){
            if(qui.equals(nom[i])){
                a = "La moyenne d'age est de: " + AgeMoyenne(age) + " ans, l'age de " + qui + " est de " + age[i] +
                " et il est dans la catégorie " + Categorie(age[i]) + " sachant que le plus petit a " + AgeMin(age) +
                " et le plus grand a " + AgeMax(age);
            }
        }
        return a;
    }
     public static void main(String[] args) {
         Scanner sc = new Scanner(System.in);

         int x = 1;
         while(x == 1) {
             System.out.println("Recherchez-vous quelqu'un ? Veuillez saisir son nom.");
             String nom = sc.next();
             System.out.println(Utilisateurs(nom));

             System.out.println("Voulez-vous continuer? 1 = Oui & 0 = Non");
             x = sc.nextInt();
         }


    }
}
