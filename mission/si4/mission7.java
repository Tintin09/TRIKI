/*
Algo: Mission7
Début
Variables: nom Tableau de carractère
           note Tableau d'entier
		   eleve Chaine de carractère
		   moyenne Tableau de réel
		   x,i entier
		   moy réel

	Saisir eleve
	nom[] <- {"Anis","Anis2","Anis3"}
	note[][] <- {{1,2,3},{1,2,3},{1,2,3}}

	x <- 0
	moy <- 0

	Pour i = 0 allant de i à nom.taille pas 1
	    faire
		    Tant que x x note.taille
			    alors
				moy <- moy + note[i][x]
				x <- x + 1
			Fin tant que
			
			moyenne[i] <- moy / nom.taille
			moy = 0
			x = 0
			Si eleve == nom[i] faire
			    Tant que x < note[i].taille faire
				    Afficher note[i][x]
					x <- x + 1
				Fin Tant que
				Afficher moyenne[i]
			Sinon faire
			    Afficher -1
				
			Fin Si
	Fin Pour
FIN
					
 */
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("écrire le nom de l'élève:");
        String eleve = sc.next();


        String nom[] = {"Anis","Anis2","Anis3"};
        int note[][] = {{1,2,3},{1,2,3},{1,2,3}};
        double[] moyenne = new double[nom.length];
        int x = 0;
        double moy = 0;
        for (int i = 0; i < nom.length; i++){
            while (x < note[i].length){
                moy = moy + note[i][x];
                x++;
            }
            moyenne[i] = moy / nom.length;
            moy = 0;
            x = 0;
            if(eleve.equals(nom[i])){
                while (x < note[i].length){
                    System.out.println("Une note de " + note[i][x]);
                    x++;
                }
                System.out.println("La moyenne de l'élève est de : " + moyenne[i]);
            } else {
                System.out.println(-1);
            }
        }
    } 
}
