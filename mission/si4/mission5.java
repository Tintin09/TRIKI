---------------------------------------------------------------------------------------------
Question 1 et 2:
---------------------------------------------------------------------------------------------
/*
Algo:Mission5Qet2
Debut:
variables: save,numero,x,compteur entier
		   text,signe Chaine de carractère
		   
    Pour x de [0 à 9] pas 1
	    faire
		   Saisir numero
		   
		   Si compteur <= 4 
		      alors
			    Saisir signe
			    compteur <- compteur + 1
		   Sinon
		        Signe <- "+"
		   Fin Si
		   
		   Si signe = "+" 
		      alors
			    save <- save + numero
		   Sinon
		        save <- save - numero
		   Fin Si
		   
		   Si text = ""
		      alors
			    text <- text + numero
		   Sinon
		        text <- text + signe + numero
		   Fin Si
		   
		   Si x = 9
		      alors
			    Afficher text
				Afficher "Le résultat est de" + save
		   Fin Si
    Fin Pour
FIN
*/
import java.util.Scanner;
public class Mission5 {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int save=0,numero, compteur = 0;
        String text = "",signe = "+";
        for (int x=0;x<=9;x++) {
            System.out.println(x+"- Saisissez le nombre:");
            numero = sc.nextInt();
            if(compteur <= 4) { // Car for prend en compte le 0
                System.out.println("Voulez vous additioner ou soutraire le prochain nombre? (+ ou -)");
                signe = sc.next();
                compteur++;
            } else {
                signe = "+";
            }
            if(signe == "+") {
                save = save + numero;
            } else {
                save = save - numero;
            }


            if(text == "") {
                text = text + numero;
            }
            else {
                text = text + signe + numero;
            }
            if(x == 9) {
                System.out.println(text);
                System.out.println("Le résultat est de " + save);
            }

        }

    }
}

------------------------------------------------------------------------------------------------
Question 3:
------------------------------------------------------------------------------------------------
/*
Algo:Mission5Q3
Debut:
variables:x,ValeurMax <- 101,ValeurMin <- 0,tentatives <- 0,chiffre <- 0, valeur <- Hasard [0,100] entier

	Pour x de [0 à 12] pas 1
        Saisir chiffre
        Si chiffre < 0 ou > 100
            alors
			  Afficher "Taille incorrect"
		Sinon
		  Si tentatives = 12
		    alors
			  Afficher "Plus d'éssai"
			  stop
	      Sinon Si chiffre != valeur
		    alors
		      Afficher "Chiffre incorrect"
		  Sinon
		      Afficher "Bravo vous avez gagner!"
			  stop
		  Fin Si
		  
		  tentatives <- tentatives + 1
		  
		Fin Si
	    Fin Sinon
	Fin Pour
FIN
			  */
import java.util.Scanner;
import java.util.Random;
public class Mission5 {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        Random r = new Random();
        int valeurMax = 101, valeurMin = 0, valeur = valeurMin + r.nextInt(valeurMax - valeurMin),tentatives = 0,chiffre = 0;
        for (int x=0;x<=12;x++) {
            System.out.println("Essai numéro " +x+"- Veuillez entrer un chiffre");
            chiffre = sc.nextInt();
            if(chiffre < 0 || chiffre > 100){
                System.out.println("La taille du chiffre est incorrect, réessayer.");
            } else {
                if(tentatives == 12){
                    System.out.println("Vous n'avez plus d'essai, vous avez perdu.");
                    break;
                }  else if(chiffre != valeur){
                    System.out.println("Le chiffre n'est pas correct.");
                } else {
                    System.out.println("Bravo! Vous avez gagner!");
                    break;
                }

                tentatives++;
            }

        }
    }
}

