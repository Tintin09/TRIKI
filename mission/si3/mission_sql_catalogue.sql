-- Question numéro 1
CREATE TABLE sql_mission_c_collection (
numero int primary key,
datelancement date,
nom varchar(30),
harmonie varchar(25),
);

CREATE TABLE  sql_mission_c_produit (
ref varchar(3),
designation varchar(35),
couleur varchar(25),
dimensions varchar(15),
prix double,
collection int(11),
KEY collection (collection),
CONSTRAINT sql_mission_c_produit FOREIGN KEY (collection) REFERENCES sql_mission_c_collection (numero)
);

INSERT INTO sql_mission_c_collection VALUES
(1,'2005-04-01','Marée Haute','Bleu'),
(2,'2005-04-15','Soleil','Jaune');

INSERT INTO sql_mission_c_produit VALUES
('A12','Chaise longue','Marine','120x60','90.00','1'),
('B32','Parasol','Paille','diam 90','45.00','2'),
('A14','Drap de bain','Orangé','130x80','65.00','2'),
('B25','Parasol','Ciel','diam 110','60.00','1'),
('A15','Coussin','Paille','30x30','12.00','2');


--Question 2
UPDATE sql_mission_c_produit SET prix = 65 WHERE ref = "B25";

--Question 3
UPDATE sql_mission_c_produit SET designation = "Serviette de bain" WHERE ref = "A14";

--Question 4
DELETE FROM sql_mission_c_produit WHERE designation = "Parasol";

--Question 5
INSERT INTO sql_mission_c_collection VALUES ('3','2011-10-18','Vent Pourpre','rouge');

--Question 6
-- Toutes les références comprennant B ont été supp mais je fait quand même la fonction
UPDATE sql_mission_c_produit SET collection = 2 WHERE ref LIKE "B%";
