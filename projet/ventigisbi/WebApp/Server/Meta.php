<?php

class Meta
{
    public static function fetch($page, $title = null, $description = null, $image = null)
    {
        echo '
		<!--
		 © Copyright Ventigisbi 2018. Ventigisbi Network V-' . VERSION . ' dessiné et développé par Anis pour Ventigisbi. 
		 ______________________________________________________________________________________________________
		 
		 -->
        <!DOCTYPE html>
		<html lang="fr">
        <head>
        <base href="' . URL . '" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="robots" content="index,follow,all" />
        <meta name="viewport" content="width=device-width,initial-scale=0.3">
        <meta name="theme-color" content="#F9D046" />
        <meta name="keywords" content="ventigisbi" />
        <meta name="Geography" content="France" />
        <meta name="country" content="France" />
        <meta name="Language" content="French" />
        <meta name="identifier-url" content="' . URL . '" />
        <meta name="Copyright" content="Ventigisbi" />
        <meta name="language" content="fr-FR" />
        <meta name="hreflang" content="fr-FR" />
        <meta name="category" content="Website">
        <meta property="og:site_name" content="Ventigisbi" />
        <meta property="og:image" content="' . self::getImage($page, $image) . '" />
        <meta property="og:locale" content="fr_FR" />
        <meta property="og:url" content="https://' . $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] . '"/>
        <meta property="og:type" content="' . $page . '" />
        <meta property="og:description" content="' . self::getDescription($page, $description) . '" />
        <meta property="og:title" content="' . self::getTitle($page, $title) . '" />
        <meta name="description" content="' . self::getDescription($page, $description) . '" />
        <link rel="shortcut icon" href="' . URL . 'WebApp/Template/Images/icone.png">
        <link rel="stylesheet" href="WebApp/Template/WebApp.css?'.VERSION.'" type="text/css" media="all" />
        <title>' . self::getTitle($page, $title) . '</title>
        </head>';
    }
	
	public function getTitle($page, $string = null)
    {
       switch($page) 
	   {		   
		   case 'index': 
			$data = "Ventigisbi";
			break;
			
			default:
			$data = "Ventigisbi";
			break;
			
	   }
	   
	   return $data;
    }
	
	public function getDescription($page, $string = null)
    {
       switch($page) 
	   {
		    case 'index':
			$data = "Ventigisbi - La vente de médicaments !";
			break;
			
			default:
			$data = "Ventigisbi - La vente de médicaments !";
			break;
	   }
	   
	   return $data;
    }
	
	public function getImage($page, $string = null)
    {
       switch($page) 
	   {
		   
		   case 'index': 
			 $data = URL ."swfs/c_images/upload/2Ozu040OwTj43t9.png";
			 break;
			default: 
			  $data = URL ."swfs/c_images/upload/h1488387535bc.png";
			break;			
	   }
	   
	   return $data;
    }
}

?>