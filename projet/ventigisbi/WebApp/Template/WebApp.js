var panier = {
    prix: 0,
    produits: [],
    countProduits: 0,
    setProduit: function (id, prix, quantite, image, nom) {
        var nouveau = true;
        for (var i = 0; i < this.produits.length; i++) {
            if (this.produits[i][0] == id) {
                nouveau = false;
                var carre = i;
            }
        }
        if (nouveau) {
            this.produits.push([id, prix, quantite, image, nom]);
            $("#v41").fadeOut(0).prepend('<div onclick="panier.deleteProduit(' + id + ')" class="produit' + id + '" id="v42"><div id="v43"><img id="v44" src="' + image + '"/></div><div id="v45">' + nom + '</div><div id="v46"><div id="v47">' + prix + '€</div><div id="v48">x' + quantite + '</div></div></div>').fadeIn();
            this.countProduits = this.countProduits + 1;
            var count = $("#v7").text();
            $("#v7").text(parseInt(count) + 1);
        } else {
            var unite = this.produits[carre][2] + quantite;
            this.produits[carre][2] = unite;
            this.setUnite(unite, this.produits[carre][0]);
        }
        this.prix = this.prix + (quantite * prix);
        this.setPrix(this.prix);
        this.setCookie();
    },
    deleteProduit: function (id) {
        for (var i = 0; i < this.produits.length; i++) {
            if (this.produits[i][0] == id) {
                var prix = this.produits[i][1] * this.produits[i][2];
                this.prix = this.prix - prix;
                this.setPrix(this.prix);
                this.produits.splice(i, 1);
                $(".produit" + id).fadeOut();
                this.countProduits = this.countProduits - 1;
                var count = $("#v7").text();
                $("#v7").text(parseInt(count) - 1);
                this.setCookie();
            }
        }
    },
    setPrix: function (nouveauprix) {
        var divprix = $("#v37"),
            prixactuel = parseInt(divprix.html());

        function add() {
            setTimeout(function () {
                if (prixactuel < nouveauprix) {
                    prixactuel++;
                    divprix.html(prixactuel);
                    add();
                } else {
                    divprix.html(prixactuel + "€");
                    return;
                }


            }, 0)
        }

        function remove() {
            setTimeout(function () {
                if (prixactuel > nouveauprix) {
                    prixactuel--;
                    divprix.html(prixactuel);
                    remove();
                } else {
                    divprix.html(prixactuel + "€");
                    return;
                }


            }, 0)
        }

        if (parseInt(divprix.html()) < nouveauprix) {
            add();
        } else {
            remove();
        }
    },
    setUnite: function (unite, id) {
        $(".produit" + id + " #v48").html("x" + unite);
    },
    getCookie: function (cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    },
    checkCookie: function (data) {
        var cookie = this.getCookie(data);
        if (cookie != "") {
            return true;
        } else {
            return false;
        }
    },
    setCookie: function () {
        var d = new Date();
        d.setTime(d.getTime() + (3 * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        prix = this.prix;
        document.cookie = "produits" + "=" + JSON.stringify(this.produits) + ";" + expires + ";path=/";
        document.cookie = "prix=" + prix + ";" + expires + ";path=/";
        document.cookie = "countProduits" + "=" + this.countProduits + ";" + expires + ";path=/";
    },
    fetchCookie: function () {
        this.produits = JSON.parse(this.getCookie('produits'));
        this.prix = parseInt(panier.getCookie('prix'));
        this.countProduits = parseInt(this.getCookie('countProduits'));
        this.setPrix(this.prix);
        for (var i = 0; i < this.produits.length; i++) {
            $("#v41").fadeOut(0).prepend('<div onclick="panier.deleteProduit(' + this.produits[i][0] + ')" class="produit' + this.produits[i][0] + '" id="v42"><div id="v43"><img id="v44" src="' + this.produits[i][3] + '"/></div><div id="v45">' + this.produits[i][4] + '</div><div id="v46"><div id="v47">' + this.produits[i][1] + '€</div><div id="v48">x' + this.produits[i][2] + '</div></div></div>').fadeIn();
        }

    },
    fetch: function(){
        $("html").animate({
            left: '-400px'
        },200);
        $("#v34").animate({
            right: '0px'
        },200);
    },
    close: function(){
        $("html").animate({
            left: '0px'
        },200);
        $("#v34").animate({
            right: '-400px'
        },200);
    }

}
if (panier.checkCookie('produits')) {
    panier.fetchCookie();
    $("#v7").text(panier.countProduits);
}