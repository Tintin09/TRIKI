<?php
  // DEFINE
  define('URL', 'http://localhost/mypfolio/');
  define('ROOTDIR', __DIR__);

  define('CLASSDIR', ROOTDIR . '/app/class/');
  define('POWER', 'portfolio');
 
  ('UTF-8');
  setlocale(LC_TIME, 'fr','fr_FR','fr_FR@euro','fr_FR.utf8','fr-FR','fra');

  // CLASS LOADER
  include CLASSDIR . 'Autoloader.class.php';

  Autoloader::load();
  $db = new Database;
  $system = new System;
  $html = new Html; 
?>